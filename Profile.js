import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Modal, TouchableOpacity } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Profile extends React.Component {

    state = {
        isShowModal: false,
        username: '',
        message: ''
    };

    goToScreen1 = () => {
        this.props.history.push('/list', {
        })
    }

    goToEdit = () => {
        this.props.history.push('/edit', {
            myusername: this.props.location.state.myusername,
            mypassword: this.props.location.state.mypassword,
           

        })
    }

    onShowModal() {
        this.setState({ isShowModal: true });
    }

    onHideModal() {
        this.setState({ isShowModal: false });
    }

    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <View style={styles.back}>
                        <TouchableOpacity onPress={this.goToScreen1}>
                            <Text style={styles.headerText}>Back</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.profileBar}>
                        <Text style={styles.headerText}>My Profile</Text>
                    </View>
                </View>

                <View style={styles.content}>


                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <View >
                                <Text style={styles.headerText}>Username: {this.props.location.state.myusername}</Text>
                                <Text style={styles.headerText}>Password:{this.props.location.state.mypassword} </Text>
                                <Text style={styles.headerText}>First name:{this.props.location.state.myfirstname} </Text>
                                <Text style={styles.headerText}>Lastname:{this.props.location.state.mylastname} </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <View style={styles.edit}>
                        <TouchableOpacity onPress={this.goToEdit}>
                            <Text style={styles.headerText}>Edit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    profileBar: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 4,
        padding: 20


    },

    back: {
        backgroundColor: '#82E0AA',
        flex: 0.2,
        margin: 4,
        padding: 20

    },

    edit: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 10,
        padding: 20

    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'

    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    footer: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },
    logo: {
        borderRadius: 30,
        width: 150,
        height: 150
    },
    modalLayout: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },
    textStyle: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    }


})
export default Profile
