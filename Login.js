import React, { Component } from '../../../AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/react';
import { StyleSheet, Text, View, TextInput, ScrollView, Image, Button, Alert, TouchableOpacity } from 'react-native';
import { Link } from '../../../AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/react-router-native'



class Login extends React.Component {

    state = {
        username: '',
        password: ''
    };

    onClickLogin = () => {
        // const { addTodo } = this.props
        this.props.history.push('/list', this.props.addTodo(this.state.username))

    }


    goToScreen1 = () => {
        this.props.history.push('/list', {
            myusername: this.state.username,
            mypassword: this.state.password
        })
    }


    render() {
        const {account, addTodo } = this.props
        console.log(account)
        return (

            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.layout1, styles.center]}>
                        <Text style={[styles.center, styles.headerStyle]}>
                            Login Page
                    </Text>
                        <View style={[styles.logo, styles.center]}>
                            <Image source={require('./profile2.jpg')} style={[styles.logo]} />
                        </View>
                    </View>

                    <Text> {this.state.username}-{this.state.password} </Text>
                    <View style={[styles.layout2, styles.center]}>
                        <Text style={styles.style}>Username :</Text>
                        <View style={[styles.textInput, styles.center, styles.radius]}>

                            <TextInput
                                style={styles.style}
                                onChangeText={(username) => this.setState({ username })}>

                            </TextInput>
                        </View>

                        <Text style={styles.style}>Password :</Text>
                        <View style={[styles.textInput, styles.center, styles.radius]}>
                            <TextInput
                                style={styles.style}
                                onChangeText={(password) => this.setState({ password })}>

                            </TextInput>
                        </View>

                        <View style={[styles.touchable, styles.radius, styles.center]}>

                            <TouchableOpacity onPress={this.goToScreen1}>
                                <Text style={styles.textLogin}>Login</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </View>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        products: state.products
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (username) => {
            dispatch({
                type: 'ADD_ACCOUNT',
                username: username,
                firstname: '',
                lastname: ''
            })
        },
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#212F3C',
        flex: 1
    },

    content: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout1: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    layout2: {
        backgroundColor: '#212F3C',
        flex: 1,
        flexDirection: 'column'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 100,
        width: 200,
        height: 200
    },

    image: {
        backgroundColor: '#7DCEA0',
        margin: 50,
    },

    textInput: {
        backgroundColor: '#505392',
        flex: 1,
        padding: 12,
        margin: 5
    },

    textLogin: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20
    },

    touchable: {
        backgroundColor: '#263A8A',
        flex: 1,
        margin: 50,
    },
    style: {
        color: 'white',
        textAlign: 'left',
        fontSize: 17,
    },
    headerStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 27,
        margin: 20
    },
    radius: {
        borderRadius: 100,
        width: 400,
        height: 400
    }


})
export default Login
