//ไฟล์สำหรับเปลี่ยนหน้า โดยใช้tag route โดยจะประกอบด้วย3tier ลึกลงไป ได้แก่ nativerouter,switch,route
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import Login from './Login'
import List from './List'
import Profile from './Profile'
import Edit from './Edit'
import AddProduct from './AddProduct'
import Product from './Product'
import EditProduct from './EditProduct'
// import { store, history } from './AppStore.js'
// import { Provider } from 'react-redux'
// import App from './App.js'


class Router extends Component {
    render() {
        return (
            <NativeRouter>
                {/* <Provider> */}
                {/* <ConnectedRouter history={history}> */}
                <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/list" component={List} />
                    <Route exact path="/profile" component={Profile} />
                    <Route exact path="/edit" component={Edit} />
                    <Route exact path="/addproduct" component={AddProduct} />
                    <Route exact path="/product" component={Product} />
                    <Route exact path="/editproduct" component={EditProduct} />
                    <Route exact path="/screen1" component={Screen1} />
                    <Route exact path="/screen2" component={Screen2} />
                    <Redirect to="/login" />
                </Switch>
                {/* </Provider> */}
                {/* </ConnectedRouter history={history}> */}
            </NativeRouter>
        )
    }
}
export default Router