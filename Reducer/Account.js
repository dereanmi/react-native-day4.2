const accountReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_ACCOUNT':
            return {
                username: action.username,
                firstname: action.firstname,
                lastname: action.lastname
            }
        case 'EDIT_ACCOUNT':
            return {
                ...state,
                ...payload
            }
    }
}

const editProductAction = (payload) => {

    return {
        type: 'EDIT_ACCOUNT',
        payload
    }
}

dispatchEvent(editProductAction ({
    name: ''
}))

const productsReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_ACCOUNT':
            return [...state, productReducer(null, action)]
        case 'EDIT_ACCOUNT':
            return state.map((each) => {
                if (each.username === action.username) {
                    return productReducer(each, action)
                }
                return each
            })
        default:
            return state
    }
}
