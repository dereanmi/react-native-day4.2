import React, { Component } from 'react';
import { Text, View, Button, StyleSheet, TouchableOpacity, TextInput } from 'react-native';


class Product extends Component {
    state = {
        username: '',
        inputUsername: '',
        inputPassword: '',
        inputFirstname: '',
        inputLastname: '',
        inputProductImage: "",
        inputProductName: "",
    }

    goToScreen1 = () => {
        this.props.history.push('/list', {
            myproductimage: this.state.inputProductImage,
            myproductname: this.state.inputProductName
        })
    }

    goToAddProduct = () => {
        this.props.history.push('/addproduct', {
            myproductimage: this.state.inputProductImage,
            myproductname: this.state.inputProductName
        })
    }

    onEditProduct = () => {
        this.props.history.push('/editproduct', {
            myproductimage: this.state.inputProductImage,
            myproductname: this.state.inputProductName
        })
    }

    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <View style={styles.back}>
                        <TouchableOpacity onPress={this.goToAddProduct}>
                            <Text style={[styles.headerText, styles.center]}>Back</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.profileBar}>
                        <Text style={[styles.headerText, styles.center]}>Product</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>

                        <View style={styles.box1}>

                            <View >
                                <Text style={styles.headerText}>Image: {this.props.location.state. myproductimage}</Text>
                                <Text style={styles.headerText}>Name:{this.props.location.state.myproductname} </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <View style={styles.save}>
                        <TouchableOpacity onPress={this.onEditProduct}>
                            <Text style={styles.headerText}>Edit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    profileBar: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 4,
        padding: 20
    },

    back: {
        backgroundColor: '#82E0AA',
        flex: 0.2,
        margin: 4,
        padding: 20

    },

    save: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 10,
        padding: 20

    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'row'
    },

    box1: {
        flex: 1,
        margin: 14,
        // alignItems: 'center',
        // justifyContent: 'center'
    },

    textInput: {
        backgroundColor: '#FCF3CF',
        // //flex: 1,
        // padding: 10,
        //margin: 20,

    },

    footer: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
    },

    logo: {
        borderRadius: 30,
        width: 150,
        height: 150
    },
    modalLayout: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },
    textStyle: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    radius: {
        borderRadius: 50,
        width: 350,
        height: 70
    }


})

export default Product
